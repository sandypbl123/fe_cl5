import React, {  useEffect } from 'react';
import { useNavigate } from "react-router-dom";
// import authContenxt from '../utils/auth-context';
import "../login.css";
function  LoginRedirect() {
    const Navigate=useNavigate();
    // called reducer
    // const authCtx = useContext(authContenxt);
    // console.log("Reloq");
    useEffect(() => {
        // request outdata 
        const getData = async () => {
            try {
                const request = await fetch("http://localhost:5000/auth/login/success", {
                    method: "GET",
                    credentials: "include",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Credentials": true,
                    }
                });
                if (request.status === 200) {
                    const getData = await request.json();
                    // authCtx.login(getData);
                    // console.log(getData);
                    localStorage.setItem("tokenJWT",getData.tokenJWT);
                   return Navigate("/home");
                
                }
                throw new Error("Opps,Error when request")
                // console.log(request);
            } catch (e) {
                console.log(e.message);
            }
        }
        getData();
    }, [])

    return (
        <div className="h-[100vh]">
            <div className="w-full h-full fixed -z-10" style={{ backgroundColor: "white", opacity: ".9" }}>as</div>
            <div className="flex justify-center items-center z-40 h-full ">
                <div className="bg-white px-40 py-8 rounded-xl flex items-center" style={{ flexDirection: "column" }}>
                    <svg
                        className="animate-spin -ml-1 mr-3 h-40 w-40 text-main"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                    >
                        <circle
                            className="opacity-25"
                            cx="12"
                            cy="12"
                            r="10"
                            stroke="currentColor"
                            strokeWidth="4"
                        ></circle>
                        <path
                            className="opacity-75"
                            fill="currentColor"
                            d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                        ></path>
                    </svg>
                    <h1 className="text-center font-semibold text-3xl mt-10 ">Redirecting...</h1>
                </div>
            </div>
        </div>
    )
}

export default LoginRedirect