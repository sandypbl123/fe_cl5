import "../login.css";
import logo from "../assets/tmovie.png";
import { useRef,useEffect,useState } from "react";
import { useNavigate } from "react-router-dom";
// import crypto from "crypto";
const Login = (props) => {
  const Navigate=useNavigate();
  const refEmail=useRef(); 
  const refPassword=useRef(); 
  const [changeAlert,setChangeAlert]=useState(false);
  const refAlert=useRef();
  // console.log(refAlert.current.classList.add("tes"));
  useEffect(()=>{
    if(changeAlert){
      console.log("run");
      // display alert
      refAlert.current.classList.remove("hidden");
      setTimeout(()=>{
        refAlert.current.classList.add("transitionRight");
      },
      1000)
      setTimeout(()=>{
        refAlert.current.classList.add("hidden");
        refAlert.current.classList.remove("transitionRight");
        setChangeAlert(current=>!current)
      },
      1100)
    }
  }
  ,[refAlert,changeAlert])
  document.body.classList.add("body");
  const dummyHandler=e=>{
    e.preventDefault();
    const defaultEmail="login@gmail.com";
    const defaultPassword="login";
    const getEmailFromUser=refEmail.current.value;
    const getPassowrdFromUser=refPassword.current.value;
    if(defaultEmail ===getEmailFromUser &&defaultPassword === getPassowrdFromUser){
      localStorage.setItem("tokenJWTForDummy","43a9a84f8c73e7056bc8b9084d3eb2e518c866d432ae0598cce7fcc976500dbe9e29fbaa1fd0541cc869b55f18df2137da349333972e939a557a628a97ec0ab4");
      // console.log(Navigate());
     return Navigate("/home");
    }else{
      console.log("false");
      setChangeAlert(current=>!current);
    }
    
  }
  const handleGoogleLogin=(e)=>{
    e.preventDefault();
    // alert("WORKS");
    // redirect("http://localhost:5000/auth/google");
    window.open("http://localhost:5000/auth/google","_self");
  }
  return (
    <>
      <div className="container-full w-full h-[100vh]">
        <div className="lg:grid lg:grid-cols-[45%,55%] lg:justify-center lg:h-[100vh] w-full">
          {/* <div className="w-full h-full lg:overflow-y-scroll scrollbar lg:pt-20 lg:h-[100vh] grid grid-cols-1 justify-center grid-rows-[10rem] lg:grid-rows-[15rem] pb-20 pt-10 box-border bg-page-login">
            <div id="logo" className="flex justify-center">
              <img
                src={logo}
                className="h-32 md:h-40 lg:h-52"
                alt=""
              />
            </div>
            <h2 className="text-center text-2xl font-bold  lg:text-3xl">
              Welcome To Movie
            </h2>
            <div className="px-4 lg:px-8 mt-4">
              <p className="text-center font-normal text-base lg:text-md">
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias,
                est deleniti repellat quos sunt minus consequatur commodi
                distinctio iusto numquam? Consectetur facilis minima, quaerat
                quod, libero similique expedita quisquam odit debitis ipsam ad
                quidem. Sapiente deleniti fugiat nulla magni totam. Optio atque
                libero eos quod facere vero iste maxime a?
              </p>
              <p className="text-center font-normal text-base lg:text-md mt-2">
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias,
                est deleniti repellat quos sunt minus consequatur commodi
                distinctio iusto numquam? Consectetur facilis minima, quaerat
                quod, libero similique expedita quisquam odit debitis ipsam ad
                quidem. Sapiente deleniti fugiat nulla magni totam. Optio atque
                libero eos quod facere vero iste maxime a?
              </p>
              <p className="text-center font-normal text-base lg:text-md mt-2">
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias,
                est deleniti repellat quos sunt minus consequatur commodi
                distinctio iusto numquam? Consectetur facilis minima, quaerat
                quod, libero similique expedita quisquam odit debitis ipsam ad
                quidem. Sapiente deleniti fugiat nulla magni totam. Optio atque
                libero eos quod facere vero iste maxime a?
              </p>
            </div>
          </div> */}
          <div className="w-full flex flex-col gap-y-8 justify-center items-center">
          <p ref={refAlert} className=" hidden timeTransitionAlert font-semibold block md:w-[60%] md:displayAlert transition" style={{backgroundColor:"red",color:"white",borderRadius:"1rem"}}>Oops Error, Email and Passowrd found</p>
            <div id="title-login">
              <h2 className="font-semibold text-xl text-center  lg:text-2xl">
                Login Page Movie
              </h2>
              <h2 className="font-semibold text-xl text-center uppercase lg:text-2xl">
                <span className="text-main tracking-wider">Movie-List</span>{" "}
              </h2>
            </div>
            <div id="form" className="md:w-[60%]">
              <form onSubmit={dummyHandler} action="" className="w-full">
                <div className="mt-2">
                  <label
                    for="Username"
                    className="font-semibold text-smlg:text-medium  mb-2"
                  >
                    Email
                  </label>
                  <input
                  ref={refEmail}
                    type="email"
                    id="username"
                    name="username"
                    required
                    className="block w-full px-4 py-2 rounded-md bg-slate-200 border-none focus:border-none focus:ring-main focus:ring-2"
                    placeholder=""
                  />
                  <span
                    id="usernameMessage"
                    className="hidden text-pink-500 text-sm"
                  ></span>
                </div>
                <div className="mt-6">
                  <label
                    for="Username"
                    className="font-semibold text-sm lg:text-medium mb-2"
                  >
                    Password
                  </label>
                  <input
                  ref={refPassword}
                    type="password"
                    id="password"
                    name="password"
                    className="block px-4 py-2 w-full rounded-md bg-slate-200 border-none focus:border-none focus:ring-main focus:ring-2"
                    placeholder=""
                    required
                  />
                  <span
                    id="passwordMessage"
                    className="hidden text-pink-500  text-sm "
                  ></span>
                </div>

                <button

                  id="submit"
                  type="submit"
                  className=" px-4 py-2  mt-8 w-full text-base font-semibold  bg-main rounded-md border-none text-white btn-flash  after:rounded-md after:hover:w-full"
                >
                  Login
                </button>
                <div>
                  <h2 className="text-center mt-8 text-semibold">
                    Social Media Methode Login
                  </h2>
                  <hr />
                  <button 
                    className=" flex items-center gap-4 justify-center px-4 py-2  mt-8 w-full text-base font-semibold  bg-emerald-400 rounded-md border-none text-white btn-flash  after:rounded-md after:hover:w-full"
                    onClick={handleGoogleLogin}
                  >
                    <span className="font">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        x="0px"
                        y="0px"
                        width="20"
                        height="20"
                        viewBox="0 0 48 48"
                      >
                        <path
                          fill="#FFC107"
                          d="M43.611,20.083H42V20H24v8h11.303c-1.649,4.657-6.08,8-11.303,8c-6.627,0-12-5.373-12-12c0-6.627,5.373-12,12-12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C12.955,4,4,12.955,4,24c0,11.045,8.955,20,20,20c11.045,0,20-8.955,20-20C44,22.659,43.862,21.35,43.611,20.083z"
                        ></path>
                        <path
                          fill="#FF3D00"
                          d="M6.306,14.691l6.571,4.819C14.655,15.108,18.961,12,24,12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C16.318,4,9.656,8.337,6.306,14.691z"
                        ></path>
                        <path
                          fill="#4CAF50"
                          d="M24,44c5.166,0,9.86-1.977,13.409-5.192l-6.19-5.238C29.211,35.091,26.715,36,24,36c-5.202,0-9.619-3.317-11.283-7.946l-6.522,5.025C9.505,39.556,16.227,44,24,44z"
                        ></path>
                        <path
                          fill="#1976D2"
                          d="M43.611,20.083H42V20H24v8h11.303c-0.792,2.237-2.231,4.166-4.087,5.571c0.001-0.001,0.002-0.001,0.003-0.002l6.19,5.238C36.971,39.205,44,34,44,24C44,22.659,43.862,21.35,43.611,20.083z"
                        ></path>
                      </svg>
                    </span>
                    Google
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Login;
