// import React from "react";

// import 'swiper/dist/js/swiper.js'

import "swiper/swiper.min.css";
import "./assets/boxicons-2.0.7/css/boxicons.min.css";
import "./App.scss";

import {
  BrowserRouter,
  Routes,
  Route,
  createBrowserRouter,
  Outlet,
  RouterProvider,
} from "react-router-dom";

import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Catalog from "./pages/Catalog";
import Home from "./pages/Home";
import Login from "./pages/Login";

import Routing from "./config/Routing";

import LoginRedicet from "./pages/LoginRedirect";

function App() {
  const Layout = () => {
    return (
      <>
        <Header />
        <Outlet />
        <Footer />
      </>
    );
  };
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Layout />,
      children: [
        {
          path: "",
          element: <Home />,
        },
        {
          path: "/Home",
          element: <Home />,
        },
        {
          path: "/movie",
          element: <Catalog />,
        },
      ],
    },
    {
      path:"/login",
      element:<Login/>
    },
    {
      path:"/loginRedirect",
      element:<LoginRedicet/>
    }
  ]);
  return (
    <div>
      <RouterProvider router={router} />
    </div>
);
}

export default App;

// <BrowserRouter>
//   <Routes>
//     <Route
//       path="/"
//       animate={true}
//       element={
//         <React.Fragment>
//           <Header />

//           <Footer />
//           <Routing />
//         </React.Fragment>
//       }
//     />

//     <Route
//       path="/Movie"
//       element={
//         <React.Fragment>
//           <Catalog />
//         </React.Fragment>
//       }
//     />
//   </Routes>
// </BrowserRouter>

// <BrowserRouter>
// <Header />
// <Routes>
//   <Route path="/" element={<Home />}/>
//   <Route path="/Catalog" element={<Catalog />}/>
//   <Route path="/Detail" element={<Detail />}/>
// </Routes>
// <Footer />

// </BrowserRouter>