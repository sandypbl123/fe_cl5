import{ useReducer }  from 'react';
import authContenxt from './auth-context';

const defaultLogin={
    data:[],
    statusLogin:false
}
const loginReducer=(state,action)=>{
    switch(action.type){
        case"login":
        return {
            data:action.allData.data,
            statusLogin:action.allData.statusLogin
        }


        case"logout":
        return {
            data:[],
            statusLogin:false
        }

        default:return state;
    }
}
function AuthProvider(props) {
    const[stateLogin,dispatchLogin]=useReducer(loginReducer,defaultLogin);

    // function to get Throught the context
    const userLogin=(data)=>{
        dispatchLogin({type:"login",allData:{
            statusLogin:true,
            data:data
        }})
    };
    const userLogout=()=>{
        dispatchLogin({type:"logout"});
    }
    const authContext={
        data:stateLogin.data,
        statusLogin:stateLogin.statusLogin,
        login:userLogin,
        logout:userLogout
    }
    // console.log(authContext.statusLogin);
  return (
    <authContenxt.Provider value={authContext}>
        {props.children}
    </authContenxt.Provider>
  )
}

export default AuthProvider;