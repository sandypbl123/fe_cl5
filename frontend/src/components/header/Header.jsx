import React, { useRef, useEffect } from 'react';

import { Link, useLocation } from 'react-router-dom';

import './header.scss';

import Button, { OutlineButton } from '../button/Button';

import logo from '../../assets/tmovie.png';

const headerNav = [
    {
        display: 'Home',
        path: '/'
    },
    {
        display: 'Movies',
        path: '/movie'
    },
    {
        display: 'TV Series',
        path: '/tv'
    }
];

const Header = () => {

    const location = useLocation();
    const isHomeRoute = location.pathname === "/home"
    const { pathname } = useLocation();
    const headerRef = useRef(null);

    const active = headerNav.findIndex(e => e.path === pathname);

    useEffect(() => {
        const shrinkHeader = () => {
            if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
                headerRef.current.classList.add('shrink');
            } else {
                headerRef.current.classList.remove('shrink');
            }
        }
        window.addEventListener('scroll', shrinkHeader);
        return () => {
            window.removeEventListener('scroll', shrinkHeader);
        };
    }, []);


    // handle logout
    const logoutHandler = (e) => {
        e.preventDefault();
        localStorage.removeItem("tokenJWT");
        localStorage.removeItem("tokenJWTForDummy");
        window.open("http://localhost:5000/auth/logout", "_self");
    }

    return (
        <div ref={headerRef} className="header">
            <div className="header__wrap container">
                <div className="logo">
                    <img src={logo} alt="" />
                    <Link to="/">Movie-List</Link>
                </div>
                <ul className="header__nav">
                    {
                        headerNav.map((e, i) => (
                            <li key={i} className={`${i === active ? 'active' : ''}`}>
                                <Link to={e.path}>
                                    {e.display}
                                </Link>
                            </li>
                        ))
                    }
                </ul>

                {!isHomeRoute &&
                    <div>
                        <a href='/login'>
                            <Button>Login</Button>
                        </a>
                        <OutlineButton>Register</OutlineButton>
                    </div>
                }
                {isHomeRoute &&
                    <div>
                    <Link style={{width: '100%' }} onClick={logoutHandler}><h1 className='text-center'>Logout</h1></Link>
                    </div>
                }
            </div>
        </div>
    );
}

export default Header;